function showSignInData(){
    let email = document.getElementById('input-email').value;
    let password = document.getElementById('input-password').value;
    document.getElementById('user-email').textContent  = email;
    document.getElementById('user-password').textContent  = password;
}

function showSignUpData(){
    document.getElementById('modal-up-name').textContent = document.getElementById('input-up-name').value;
    document.getElementById('modal-up-lastname').textContent = document.getElementById('input-up-lastname').value;
    document.getElementById('modal-up-email').textContent = document.getElementById('input-up-email').value;
    document.getElementById('modal-up-pw').textContent = document.getElementById('input-up-pw').value;
    document.getElementById('modal-up-description').textContent = document.getElementById('input-up-description').value;
}